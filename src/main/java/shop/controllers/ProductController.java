package shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import shop.model.entities.Product;
import shop.model.repositories.ProductsRepository;

import java.util.Map;

@Controller
public class ProductController {

    private ProductsRepository productsRepository;

    @Autowired
    public ProductController(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @RequestMapping("/product")
    public String redirect() {
        return "redirect:/catalog/1";
    }

    @RequestMapping("/product/{id}")
    public String cart(
            @PathVariable(name = "id", required = false)
                    Integer productId,
            Model model) {

        if (productId < 0) {
            return "redirect:/catalog/1";
        }

        Product currentProduct =
                productsRepository.findById(productId).orElse(null);

        if (currentProduct == null) {
            return "redirect:/catalog/1";
        }

        model.addAttribute("product", currentProduct);

        return "product";

    }
}
